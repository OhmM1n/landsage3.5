import React, { Component } from 'react'
import Leaflet from 'leaflet';
import moment from 'moment';
import axios from 'axios';

import mapIcon from '../asset/map-pin.png'
import rainIconSVG from '../asset/rain_green.svg'
import ubonLandusePic from "../asset/ubon-landuse-2017.png"
import ubonPopulationPic from "../asset/ubon-population-2017.png"
import mrcbound from '../data/mrcbound.json'
import mrcmekong from '../data/mrcmekong.json'



export const dischargeIcon = (fill) => new Leaflet.divIcon({
    html: `<svg xmlns="http://www.w3.org/2000/svg" width="25" height="24">
        <g>
            <path xmlns="http://www.w3.org/2000/svg" stroke="#000" id="svg_2" d="m0,11.86945l12.49178,-11.86945l12.49178,11.86945l-12.49178,11.86945l-12.49178,-11.86945z" fill="${fill || '#06F'}"/>
        </g>
        </svg>`,
    className: '',
    iconSize: [
        25, 24
    ],
    iconAnchor: [
        12.5, 24
    ],
    popupAnchor: [0, -28]
});

export const waterLevelIcon = (value) => {

    return new Leaflet.divIcon({
        html: `<svg xmlns="http://www.w3.org/2000/svg" width="25" height="24">
            <g>
            <rect xmlns="http://www.w3.org/2000/svg" stroke="#000" id="svg_4" height="22.98852" width="23.90806" y="0.3908" x="0.54596" fill="${value}"/>
            </g>
            </svg>`,
        className: '',
        iconSize: [
            25, 24
        ],
        iconAnchor: [
            12.5, 24
        ],
        popupAnchor: [0, -28]
    });
}

export const customIcon = new Leaflet.Icon({
    iconUrl: mapIcon,
    iconSize: [
        23, 32
    ],
    iconAnchor: [
        11.5, 32
    ],
    popupAnchor: [0, -34]
});

export const rainIcon = (value) => {
    let iconName = 'no_data'
    if (value <= 0) {
        iconName = 'no_rain'
    } else if (value > 0 && value <= 10) {
        iconName = 'rain_blue'
    } else if (value > 10 && value <= 35) {
        iconName = 'rain_yellow'
    } else if (value > 35 && value <= 90) {
        iconName = 'rain_orange'
    } else if (value > 90) {
        iconName = 'rain_red'
    }
    return new Leaflet.Icon({
        iconUrl: require(`../asset/${iconName}.svg`).default,
        iconSize: [
            20, 20
        ],
        iconAnchor: [
            10, 20
        ],
        popupAnchor: [0, -22]
    });
}


const beacon_icons = {
    Discharge: dischargeIcon,
    Rainfall: rainIcon,
    WaterLevel: waterLevelIcon
}


export const dateRange = () => {
    const count = 80;
    let date = [];
    for (let index = 0; index < count; index++) {
        date[index] = moment()
            .add(count * -1, 'day')
            .add(index, 'day')
            .format('DD-MM-YYYY');
    }
    return date;
}

export const RAINFALL_STATION = async () => {
    const res = await axios.get('https://api.mrcmekong.org/v1/time-series/rainfall/recent')
    if (res.data) {
        const data = res
            .data
            .features
            .map(item => {
                return {
                    id: item.properties.stationId,
                    name: item.properties.stationName,
                    position: [
                        item.geometry.coordinates[1], item.geometry.coordinates[0]
                    ],
                    ...item.properties
                }
            });
        console.log('data', data);
        return data
    } else {
        return []
    }
}

export const BEACON_LIST = async () => {
    const res = await axios.get('http://localhost:5000/data/beaconFilter/')
    if (res.data) {
        console.log('BEACON_LIST data', res.data)
        const { Data } = res.data
        let arr = []
        Data.forEach(element => {
            const { mainType, subTypes } = element
            subTypes.forEach(item => {
                arr.push({ name: `${mainType} - ${item}`, mainType, subType: item })
            });
        });
        return arr
    } else {
        return []
    }
}

export const TEXTURE_LIST = async () => {
    const res = await axios.get('http://localhost:5000/data/textureFilter/')
    if (res.data) {
        console.log('TEXTURE_LIST data', res.data)
        const { Data } = res.data
        let arr = []
        Data.forEach(element => {
            const { mainType, subTypes } = element
            subTypes.forEach(item => {
                arr.push({ name: `${mainType} - ${item}`, mainType, subType: item })
            });
        });
        return arr
    } else {
        return []
    }
}

export const getBeaconData = async (mainType, subTypes, dateFiltered) => {
    const res = await axios.get(`http://localhost:5000/data/beaconFilter/${mainType}/${subTypes}`)
    if (res.data) {
        console.log('getBeaconData data', res.data);
        const promises = res
            .data
            .map(async item => {
                const { fileList } = item
                const { custom } = fileList[0]
                let alarmLevel = 0.0
                let floodLevel = 0.0
                const customData = {}
                if (subTypes === 'WaterLevel') {
                    alarmLevel = parseFloat(custom.find(item => item.key === 'AlarmLevel').value)
                    floodLevel = parseFloat(custom.find(item => item.key === 'FloodLevel').value)
                    customData.alarmLevel = alarmLevel
                    customData.floodLevel = floodLevel
                }

                const data = await axios.get(`http://localhost:5000/data/csv/${fileList[0].fileName}`)
                const checkDate = subTypes === 'Rainfall' ? dateFiltered.format('YYYY-MM-DD') : dateFiltered.format('YYYY-MM-DD')
                let detailKeys = subTypes === 'WaterLevel' ? ['date', 'value', 'alarmLevel', 'floodLevel'] : ['date', 'value']
                if (data.data) {
                    const stationValue = data.data.data.find(item => item.Date === checkDate) || data.data.data[data.data.data.length - 1]
                    let iconValue = parseFloat(stationValue[subTypes])
                    let fill ='#666'
                    if (subTypes === 'WaterLevel') {
                        if (iconValue < customData.alarmLevel) {
                            fill = '#0f0'
                        } else if (iconValue >= customData.alarmLevel && iconValue < customData.floodLevel) {
                            fill = '#fa0'
                        } else if (iconValue >= customData.floodLevel) {
                            fill = '#f30'
                        }
                    }

                    return {
                        id: item._id,
                        country: item.country,
                        name: item.name,
                        key: `${mainType} - ${subTypes}`,
                        subTypes,
                        path: subTypes,
                        data: { ...item, date: stationValue.Date, value: iconValue, ...customData },
                        detailKeys,
                        icon: beacon_icons[subTypes](subTypes === 'WaterLevel'?fill:iconValue),
                        timeSeriesData: data.data.data,
                        position: [
                            item.lat, item.long
                        ],
                        ...item.properties
                    }
                }
            });
        const proc = await Promise.all(promises)
        return proc
    } else {
        return []
    }
}

export const getTextureData = async (mainType, subTypes) => {
    const res = await axios.get(`http://localhost:5000/texture/`)
    console.log('getTextureData data', res.data);
    if (!res.data.err) {
        const data = []
        res
            .data
            .forEach(item => {
                const { fileList, textureName, country, mainType, subType } = item
                fileList.forEach(element => {
                    const {
                        _id,
                        border,
                        date,
                        month,
                        year,
                        legendFilename,
                        textureFilename
                    } = element
                    const mapped = {
                        id: _id,
                        mainType,
                        subType,
                        country,
                        border: [
                            [
                                border.S, border.W
                            ],
                            [border.N, border.E]
                        ],
                        textureUrl: `http://localhost:5000/data/texture/${textureFilename}`,
                        legendUrl: `http://localhost:5000/data/texture/${legendFilename}`,
                        name: `${mainType} - ${subType}`,
                        date: moment(`${year}-${month}-${date}`),
                    }
                    mapped.overlay = Leaflet.imageOverlay(mapped.textureUrl, mapped.border).setOpacity(0.64);
                    data.push(mapped);
                });
            });

        return data
    } else {
        return []
    }
}

export const WATER_LEVEL = async () => {
    const res = await axios.get('http://localhost:5000/data/beacon')
    if (res.data) {
        const data = res.data
        const wt = data.filter(item => {
            return item
                .fileList
                .some(obj => obj.mainType === 'Flooding') && item
                    .fileList
                    .some(obj => obj.subType === 'WaterLevel')
        })
        const mapped = wt.map(item => {
            return {
                id: item._id,
                name: item.name,
                position: [
                    item.lat, item.long
                ],
                ...item
            }
        });
        console.log('mapped', mapped);
        return mapped
    } else {
        return []
    }
}

export const NRTH_MONITOR = async () => {
    const res = await axios.get('http://localhost:8080/nrth')
    if (res.data) {
        console.log('res.data', res.data)
        return res.data
    } else {
        return []
    }
}
export const UbonLandUseImageOverlay = Leaflet.imageOverlay(ubonLandusePic, [
    [
        14.207903, 104.370504
    ],
    [16.096864, 105.637333]
]).setOpacity(0.5);
export const UbonPopulationImageOverlay = Leaflet.imageOverlay(ubonPopulationPic, [
    [
        14.204734, 104.366532
    ],
    [16.102849, 105.645559]
]).setOpacity(1.0);
export const MRC_BOUND_GEOJSON = Leaflet.geoJSON(mrcbound, {
    style: function (feature) {
        console.log('feature', feature);
        return { color: feature.properties.stroke };
    }
});
export const MRC_MEKONG_RIVER_GEOJSON = Leaflet.geoJSON(mrcmekong, {});

export const DATA_LAYER_LIST = [
    {
        name: 'Water Level Station',
        type: 'marker',
        path: 'WaterLevel',
        icon: rainIcon(),
        function: WATER_LEVEL,
        detailKeys: ['name', 'id', 'fileList']
    }, {
        name: 'Rainfall Station',
        type: 'marker',
        path: 'RainFall',
        icon: rainIcon(),
        legendUrl: require('../asset/rainfall-legend.png'),
        function: RAINFALL_STATION,
        detailKeys: [
            'name',
            'stationId',
            'lastTimestamp',
            'last15min',
            'last24hr',
            'last7day'
        ]
    }, {
        name: 'Hydro Meteorological',
        type: 'marker',
        path: 'Hydro',
        legendUrl: require('../asset/hydro-legend.png'),
        icon: customIcon,
        function: NRTH_MONITOR,
        detailKeys: [
            'name',
            'stationId',
            'lastMeasurement',
            'waterLevel',
            'rainFall',
            'rainFall1H',
            'rainFall7to7',
            'rainFall24H'
        ]
    }, {
        name: 'MRC Bound',
        type: 'overlay',
        overlay: MRC_BOUND_GEOJSON
    }, {
        name: 'MRC MEKONG RIVER',
        type: 'overlay',
        overlay: MRC_MEKONG_RIVER_GEOJSON
    }, {
        name: 'Land used',
        type: 'overlay',
        overlay: UbonLandUseImageOverlay
    }, {
        name: 'Population',
        type: 'overlay',
        overlay: UbonPopulationImageOverlay
    }
]

/* View in fullscreen */
export function openFullscreen(elem) {
    if (elem.requestFullscreen) {
        elem.requestFullscreen();
    } else if (elem.webkitRequestFullscreen) {/* Safari */
        elem.webkitRequestFullscreen();
    } else if (elem.msRequestFullscreen) {/* IE11 */
        elem.msRequestFullscreen();
    }
}

/* Close fullscreen */
export function closeFullscreen() {
    if (document.exitFullscreen) {
        document.exitFullscreen();
    } else if (document.webkitExitFullscreen) {/* Safari */
        document.webkitExitFullscreen();
    } else if (document.msExitFullscreen) {/* IE11 */
        document.msExitFullscreen();
    }
}
