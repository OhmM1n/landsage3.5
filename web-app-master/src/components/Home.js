import React, { useState } from "react";
import { Link } from "react-router-dom";
import "./Home.css";

import { Button, Modal } from "react-bootstrap";

function Home() {
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const adminUser = {
    email: "admin@landsage.com",
    password: "password1234",
  };

  const [errorMessages, setErrorMessages] = useState({});
  const [isSubmitted, setIsSubmitted] = useState(false);

  const errors = {
    uname: "invalid email",
    pass: "invalid password",
  };

  const handleSubmit = (event) => {
    //Prevent page reload
    event.preventDefault();

    var { uname, pass } = document.forms[0];

    if (adminUser) {
      if (
        adminUser.email === uname.value &&
        adminUser.password === pass.value
      ) {
        setIsSubmitted(true);
        window.location.href = "http://localhost:3000/";
      }
      if (adminUser.email !== uname.value) {
        // Username not found/
        setErrorMessages({ name: "uname", message: errors.uname });
      } else {
        if (adminUser.password !== pass.value) {
          // Invalid password
          setErrorMessages({ name: "pass", message: errors.pass });
        }
      }
    }
  };

  // Generate JSX code for error message
  const renderErrorMessage = (name) =>
    name === errorMessages.name && (
      <div className="error">{errorMessages.message}</div>
    );

  const renderForm = (
    <Modal show={show} onHide={handleClose} backdrop="static" keyboard={false}>
      <Modal.Header>
        <Modal.Title>Admin Login</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <form onSubmit={handleSubmit}>
          <div className="mb-3">
            <label for="exampleInputEmail1" className="form-label">
              Email address
            </label>
            <input
              type="email"
              name="uname"
              className="form-control"
              id="exampleInputEmail1"
              aria-describedby="emailHelp"
            />
            {renderErrorMessage("uname")}
          </div>
          <div className="mb-3">
            <label for="exampleInputPassword1" className="form-label">
              Password
            </label>
            <input
              type="password"
              name="pass"
              className="form-control"
              id="exampleInputPassword1"
            />
            {renderErrorMessage("pass")}
          </div>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
              Close
            </Button>
            <Button type="submit" variant="primary">
              Login
            </Button>
          </Modal.Footer>
        </form>
      </Modal.Body>
    </Modal>
  );

  return (
    <>
      <div
        class="homepage"
        style={{
          backgroundImage: `url("https://assets-cdn.kathmandupost.com/uploads/source/news/2019/environment/Massive%20landslide%20blocked%20Sunkoshi%20River%2001.jpg")`,
        }}
      >
        <img
          clssName="logo-landsage"
          style={{
            marginTop: 283,
          }}
          src="https://static.wixstatic.com/media/550c90_c041177d167c4736bab2986b3aa67600~mv2.png/v1/fill/w_152,h_80,al_c,q_85,usm_0.66_1.00_0.01/LandSage-logo.webp"
          alt=""
          width="500px"
          hight="500px"
        ></img>
        <br />

        <Link to="/App">
          <a
            href="./App"
            class="btn btn-success"
            style={{
              marginTop: 10,
              marginRight: 50,
            }}
          >
            Guest
          </a>
        </Link>

        <Link
          to="/"
          className="btn btn-primary"
          style={{
            marginTop: 10,
            marginRight: 20,
          }}
          onClick={handleShow}
        >
          Admin
        </Link>
      </div>
      <>
        <div className="login-form">
          <div className="title"></div>
          {isSubmitted ? <div>User is successfully logged in</div> : renderForm}
        </div>
      </>
    </>
  );
}
export default Home;
