import React, {Component} from 'react'
import {Marker, Popup} from 'react-leaflet';
import {Divider} from 'antd';
import {Row, Col} from 'react-bootstrap';

export const DataMarker = ({
    position,
    id,
    onClick,
    markerRef,
    icon,
    isUnSync,
    data,
    emitter,
    detailKeys,
    vizPath
}) => {
    return (
        <Marker position={position} icon={icon} eventHandlers={onClick} ref={markerRef}>
            <Popup>
                <div
                    style={{
                    width: 280,
                    height: 'auto'
                }}>
                    <h6>{`Station ID : ${id}`}</h6>
                    {detailKeys.map(item => {
                        return (
                            <Row>
                                <Col xs={4}>
                                    <div
                                        style={{
                                        fontWeight: 'bold'
                                    }}>{`${item}:`}</div>
                                </Col>
                                <Col>
                                    <div
                                        style={{
                                        marginLeft: 4
                                    }}>{`${data[item]}`}</div>
                                </Col>
                            </Row>
                        )
                    })}
                    {data.fileList
                        ? (
                            <div>
                                <Divider/>
                                <p>{'File List'}</p>
                                {data
                                    .fileList
                                    .map(item => {
                                        return (
                                            <Row>
                                                <Col>
                                                    <a
                                                        onClick={() => {
                                                        window.open(`http://localhost:5000/data/${item.fileType}/${item.fileName}`, "_blank", 'location=yes,height=640,width=480,scrollbars=yes,status=yes');
                                                    }}>{item.fileName}</a>
                                                </Col>
                                                <Col>
                                                    <a
                                                        href="#"
                                                        onClick={() => {
                                                        window.open(`http://localhost:3001/${vizPath}/${item.fileName}`, "_blank", 'location=yes,height=640,width=480,scrollbars=yes,status=yes');
                                                        if (isUnSync) 
                                                            return;
                                                        emitter();
                                                    }}>{'More Details'}</a>
                                                </Col>
                                            </Row>
                                        )
                                    })}
                            </div>
                        )
                        : null}
                </div>
            </Popup>
        </Marker>
    )
}