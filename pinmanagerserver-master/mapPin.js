const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const mapPinSchema = new Schema(
  {
    name: String,
    lat: Number,
    long: Number,
    country: String,
    fileList: [
      {
        mainType: String,
        subType: String,
        date: Number,
        month: Number,
        year: Number,
        border: { N: Number, S: Number, E: Number, W: Number },
        visualFormat: String,
        fileType: String,
        fileName: String,
        custom: [{ key: String, value: String }],
      },
    ],
  },
  { timestamps: true, versionKey: false }
);
const mapPinModel = mongoose.model("MapPin", mapPinSchema);

module.exports = mapPinModel;
