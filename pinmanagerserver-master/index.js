const express = require("express");
const fileUpload = require("express-fileupload");
const config = require("./config.json");
const app = express();
var cors = require("cors");
const mongoose = require("mongoose");
const mapPin = require("./mapPin");
const textureManager = require("./textureManager");
//csv stream reader
const fs = require("fs");
const papa = require("papaparse");
const { resolveMx } = require("dns");
// const path = require("path");
//connect MongoDB
mongoose.connect(config.dbsource, {
  useNewUrlParser: true,
});

//initial express configuration
app.use(cors());
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(fileUpload());
//data checker
const wordbag = {
  country: ["TH", "VN", "KH", "LA", "MRB"],
  mainType: ["Flooding", "Landslide"],
  subType: [
    "Road",
    "Rainfall",
    "Discharge",
    "WaterLevel",
    "Population",
    "Housing",
    "Landuse",
    "Geomorphology",
    "Geology",
    "DEM",
    "Slope",
  ],
};

// initial server port
var port = process.env.PORT || 5000;
app.listen(port, () => {
  console.log("[success] task 1 : listening on port " + port);
});

// init api endpoint for responding station list
app.get("/data/beacon", async (req, res) => {
  try {
    const mapPins = await mapPin.find({});
    res.json(mapPins);
  } catch (e) {
    res.json({ err: "no station in the system" });
  }
});
// init api endpoint for responding station list
app.get("/data/texture/:fileName/border", async (req, res) => {
  const { fileName } = req.params;
  try {
    const textureManagers = await textureManager.find({});
    const allTexture = textureManagers.map((texture) =>
      texture.fileList.map((file) => file.textureFilename)
    );
    const textureArray = allTexture
      .toString()
      .split(",")
      .filter((x) => x !== "");
    console.log(textureArray);
    if (textureArray.includes(fileName)) {
      const fileDetail = textureManagers
        .map((texture) =>
          texture.fileList.find((file) => file.textureFilename === fileName)
        )
        .filter((x) => x !== undefined);

      res.json({
        border: fileDetail[0].border,
        textureFileName: fileDetail[0].textureFilename,
      });
      // const textureManagers = await textureManager.find({
      //   fileList: { $elemMatch: { textureFilename: fileName } },
      // });
      // const texture = textureManagers[0].fileList.filter(
      //   (file) => file.textureFilename === fileName
      // );
      // border = {
      //   border: texture[0].border,
      //   fileName: texture[0].textureFilename,
      // };
      // res.json(border);
    } else
      res.json({
        error:
          "texture name is not valid( or not register along with border in system)",
        validTexture: textureArray,
      });
  } catch (e) {
    res.json({ err: "no texture in the system" });
  }
});
// init api endpoint for data type finder

app.get("/data/beaconFilter/:first/:second/:third", async (req, res) => {
  try {
    let firstKey = "none";
    let secondKey = "none";
    let thirdKey = "none";
    const { first, second, third } = req.params;
    Object.entries(wordbag).forEach(([key, value]) => {
      if (value.includes(first)) {
        firstKey = key;
      }
      if (value.includes(second)) {
        secondKey = key;
      }
      if (value.includes(third)) {
        thirdKey = key;
      }
    });
    if (
      firstKey !== "none" &&
      secondKey !== "none" &&
      thirdKey !== "none" &&
      firstKey === "country" &&
      secondKey === "mainType" &&
      thirdKey === "subType" &&
      firstKey !== secondKey &&
      firstKey !== thirdKey &&
      secondKey !== thirdKey
    ) {
      const mapPins = await mapPin.find({
        [firstKey]: first,
        fileList: { $elemMatch: { [secondKey]: second, [thirdKey]: third } },
      });

      if (mapPins.length == 0 || mapPins === undefined)
        res.json({ err: "station not found" });
      else {
        mapPins.map(function (station) {
          station.fileList = station.fileList.filter(
            (file) => file.subType === third
          );
        });
        res.json(mapPins);
      }
      //else res.json({ err: "filter not found" });
    } else res.json({ err: "wrong filter word" });
  } catch (e) {
    res.json({ err: "filter not found" });
  }
});
//country must be in first  & subType must be in second
app.get("/data/beaconFilter/:first/:second", async (req, res) => {
  try {
    let firstKey = "none";
    let secondKey = "none";
    const { first, second } = req.params;
    Object.entries(wordbag).forEach(([key, value]) => {
      if (value.includes(first)) {
        firstKey = key;
      }
      if (value.includes(second)) {
        secondKey = key;
      }
    });
    if (
      firstKey !== "none" &&
      secondKey !== "none" &&
      firstKey !== "subType" &&
      secondKey !== "country" &&
      firstKey !== secondKey
    ) {
      const mapPins =
        firstKey === "country"
          ? await mapPin.find({
              [firstKey]: first,
              fileList: { $elemMatch: { [secondKey]: second } },
            })
          : await mapPin.find({
              fileList: {
                $elemMatch: { [firstKey]: first, [secondKey]: second },
              },
            });

      if (mapPins.length == 0 || mapPins === undefined)
        res.json({ err: "station not found" });
      else {
        if (firstKey === "subType" || secondKey === "subType") {
          const subT = firstKey === "subType" ? first : second;
          mapPins.map(function (station) {
            station.fileList = station.fileList.filter(
              (file) => file.subType === subT
            );
          });
        }
        res.json(mapPins);
      }
      //else res.json({ err: "filter not found" });
    } else res.json({ err: "wrong filter word" });
  } catch (e) {
    res.json({ err: "filter not found" });
  }
});
// app.get("/data/beaconFilter/:main", async (req, res) => {
//   const { main } = req.params;
//   const mapPins = await mapPin.find({
//     fileList: { $elemMatch: { mainType: main } },
//   });
//   res.json(mapPins);
// });
app.get("/data/beaconFilter/:first", async (req, res) => {
  try {
    let firstKey = "none";
    const { first } = req.params;
    Object.entries(wordbag).forEach(([key, value]) => {
      if (value.includes(first)) {
        firstKey = key;
      }
    });
    if (firstKey !== "none") {
      const mapPins =
        firstKey !== "country"
          ? await mapPin.find({
              fileList: { $elemMatch: { [firstKey]: first } },
            })
          : await mapPin.find({
              [firstKey]: first,
            });

      if (mapPins.length == 0 || mapPins === undefined)
        res.json({ err: "station not found" });
      else {
        if (firstKey === "subType") {
          mapPins.map(function (station) {
            station.fileList = station.fileList.filter(
              (file) => file.subType === first
            );
          });
        }
        res.json(mapPins);
      }
      //else res.json({ err: "filter not found" });
    } else res.json({ err: "wrong filter word" });
  } catch (e) {
    res.json({ err: "filter not found" });
  }
});

// init api endpoint for responding specific station ( for update,delete)
app.get("/data/beacon/:id", async (req, res) => {
  const { id } = req.params;
  try {
    //const mappin = await mapPin.findById(id);
    const mappin = await mapPin.find({ _id: id });
    console.log(mappin);
    if (mappin.length == 0 || mappin === undefined)
      res.json({ err: "id not found" });
    else res.json(mappin[0]);
  } catch (e) {
    res.json({ err: "station not found" });
  }
});
// init api endpoint for responding all csv file in data/csv folder
app.get("/data/", async (req, res) => {
  try {
    const csvList = fs.readdirSync(`${__dirname}/data/csv`);
    const pngList = fs.readdirSync(`${__dirname}/data/textures`);
    const fileList = [...csvList, ...pngList];
    if (fileList.length == 0 || fileList === undefined)
      res.json({ err: "no file in the system", fileList: fileList });
    else res.json({ fileList: fileList });
  } catch (e) {
    res.json({ err: "file not found" });
  }
});

//upload csv data receiver
app.post("/data/", async (req, res) => {
  if (req.files === null) {
    return res.status(400).json({ msg: "No file uploaded" });
  }
  const file = req.files.file;
  if (file.name.includes(".csv") || file.name.includes(".CSV")) {
    file.mv(`${__dirname}/data/csv/${file.name}`, (err) => {
      if (err) {
        console.error(err);
        return res.status(500).send(err);
      }
      res.json({ fileName: file.name, filePath: `data/csv/${file.name}` });
    });
  } else if (file.name.includes(".png") || file.name.includes(".PNG")) {
    file.mv(`${__dirname}/data/textures/${file.name}`, (err) => {
      if (err) {
        console.error(err);
        return res.status(500).send(err);
      }
      res.json({ fileName: file.name, filePath: `data/csv/${file.name}` });
    });
  }

  //write json csv data
  const tempfile = fs.createReadStream("data/csv/" + file.name);
  papa.parse(tempfile, {
    header: true,
    worker: true,
    complete: function (results) {
      json = { data: [...results.data] };
      var stringify = JSON.stringify(json);
      const dataName = file.name.replace(/\.[^/.]+$/, "");

      fs.writeFile(
        `${__dirname}/data/json/` + dataName + ".json",
        stringify,
        (err) => {
          if (err) {
            console.log("Cannot write file");
          }
          console.log("JSON csv data is saved.");
        }
      );
    },
  });
});
// init api endpoint for streaming csv data as json
app.get("/data/csv/:fileName", async (req, res) => {
  const { fileName } = req.params;

  fs.stat(`${__dirname}/data/csv/${fileName}`, function (err, stats) {
    console.log(stats); //here we got all information of file in stats variable

    if (err) {
      res.json({ error: "no such file" });
      return console.error(err);
    } else {
      const file = fs.createReadStream(`${__dirname}/data/csv/${fileName}`);

      papa.parse(file, {
        header: true,
        worker: true,
        complete: function (results, file) {
          res.json(results);
        },
      });
    }
  });
});
//providing csv for front end
app.get("/csv/:fileName", async (req, res) => {
  const { fileName } = req.params;

  fs.stat(`${__dirname}/data/csv/${fileName}`, function (err, stats) {
    console.log(stats); //here we got all information of file in stats variable

    if (err) {
      res.sendFile(`${__dirname}/data/textures/404.png`);
      return console.error(err);
    } else {
      res.sendFile(`${__dirname}/data/csv/${fileName}`);
    }
  });
});

app.get("/data/texture/:fileName", async (req, res) => {
  const { fileName } = req.params;

  fs.stat(`${__dirname}/data/textures/${fileName}`, function (err, stats) {
    console.log(stats); //here we got all information of file in stats variable

    if (err) {
      res.sendFile(`${__dirname}/data/textures/404.png`);
      return console.error(err);
    } else {
      res.sendFile(`${__dirname}/data/textures/${fileName}`);
    }
  });
});
//request receiver for adding station and post to database
app.post("/data/beacon", async (req, res) => {
  const payload = req.body;
  const mappin = new mapPin(payload);
  await mappin.save();
  res.send(mappin);
  //------------save json beacon file--------------------------------
  const tempfile = fs.createReadStream("data/csv/" + payload.fileName);
  papa.parse(tempfile, {
    header: true,
    worker: true,
    complete: function (results) {
      json = { data: [...results.data] };
      const pinJson = { ...payload, data: json.data };
      var stringify = JSON.stringify(pinJson);
      const dataName = payload.fileName.replace(/\.[^/.]+$/, "");

      fs.writeFile(
        `${__dirname}/data/beacon/` + dataName + ".json",
        stringify,
        (err) => {
          if (err) {
            console.log("Cannot Write file");
          }
          console.log("JSON beacon data is saved.");
        }
      );
    },
  });
});
// set -> update only changed value
// request receiver for updating station data in database
app.put("/data/beacon/:id", async (req, res) => {
  const payload = req.body;
  const { id } = req.params;
  const mappin = await mapPin.findByIdAndUpdate(id, { $set: payload });
  res.json(mappin);
});
// request receiver for deleting existing data in the database
app.delete("/data/beacon/:id", async (req, res) => {
  const { id } = req.params;
  const mappin = await mapPin.findById(id);
  if (mappin.fileList) {
    mappin.fileList.map(function (file) {
      if (file.fileName.includes(".csv") || file.fileName.includes(".CSV")) {
        fs.stat(
          `${__dirname}/data/csv/${file.fileName}`,
          function (err, stats) {
            console.log(stats); //here we got all information of file in stats variable

            if (err) {
              return console.error(err);
            }

            fs.unlink(`${__dirname}/data/csv/${file.fileName}`, function (err) {
              if (err) console.log("Cannot delete file");
              // if no error, file has been deleted successfully
              console.log("File deleted!");
            });
          }
        );
      } else {
        fs.stat(
          `${__dirname}/data/textures/${file.fileName}`,
          function (err, stats) {
            console.log(stats); //here we got all information of file in stats variable

            if (err) {
              return console.error(err);
            }

            fs.unlink(
              `${__dirname}/data/textures/${file.fileName}`,
              function (err) {
                if (err) console.log("Cannot delete file");
                // if no error, file has been deleted successfully
                console.log("File deleted!");
              }
            );
          }
        );
      }
    });
  }

  await mapPin.findByIdAndDelete(id);
  res.status(204).end();
});
// -------------------------------------------------------------------------------Texture API Section----------------------------------------------------------
app.get("/texture/", async (req, res) => {
  const textureList = await textureManager.find({});
  res.json(textureList);
});
app.post("/texture/", async (req, res) => {
  const payload = req.body;
  const texturemanager = new textureManager(payload);
  await texturemanager.save();
  res.send(texturemanager);
});
app.get("/texture/:id", async (req, res) => {
  const { id } = req.params;
  try {
    const texturemanager = await textureManager.findById(id);
    res.json(texturemanager);
  } catch (e) {
    res.json({ err: "textureSeries not found" });
  }
});
app.put("/texture/:id", async (req, res) => {
  const payload = req.body;
  const { id } = req.params;
  const texturemanager = await textureManager.findByIdAndUpdate(id, {
    $set: payload,
  });
  res.json(texturemanager);
});

app.delete("/texture/:id", async (req, res) => {
  const { id } = req.params;
  const texturemanager = await textureManager.findById(id);
  console.log(texturemanager.fileList);
  if (texturemanager.fileList) {
    texturemanager.fileList.map(function (file) {
      if (
        file.textureFilename.includes(".csv") ||
        file.textureFilename.includes(".CSV")
      ) {
        fs.stat(
          `${__dirname}/data/csv/${file.textureFilename}`,
          function (err, stats) {
            console.log(stats); //here we got all information of file in stats variable

            if (err) {
              return console.error(err);
            }

            fs.unlink(
              `${__dirname}/data/csv/${file.textureFilename}`,
              function (err) {
                if (err) console.log("Cannot delete file");
                // if no error, file has been deleted successfully
                console.log("File deleted!");
              }
            );
          }
        );
      } else {
        fs.stat(
          `${__dirname}/data/textures/${file.textureFilename}`,
          function (err, stats) {
            console.log(stats); //here we got all information of file in stats variable

            if (err) {
              return console.error(err);
            }

            fs.unlink(
              `${__dirname}/data/textures/${file.textureFilename}`,
              function (err) {
                if (err) console.log("Cannot delete file");
                // if no error, file has been deleted successfully
                console.log("File deleted!");
              }
            );
          }
        );
      }
    });
    const existLegend = texturemanager.fileList.map(
      (file) => file.legendFilename
    );
    const uniqexistLegend = [...new Set(existLegend)];
    console.log(uniqexistLegend);
    uniqexistLegend.map(function (legendfile) {
      fs.stat(
        `${__dirname}/data/textures/${legendfile}`,
        function (err, stats) {
          console.log(stats); //here we got all information of file in stats variable

          if (err) {
            return console.error(err);
          }

          fs.unlink(`${__dirname}/data/textures/${legendfile}`, function (err) {
            if (err) console.log("Cannot delete file");
            // if no error, file has been deleted successfully
            console.log("File deleted!");
          });
        }
      );
    });
  }

  await textureManager.findByIdAndDelete(id);

  res.status(204).end();
});
app.get("/textureFilter/:first/:second/:third", async (req, res) => {
  let firstKey = "none";
  let secondKey = "none";
  let thirdKey = "none";
  const { first, second, third } = req.params;
  Object.entries(wordbag).forEach(([key, value]) => {
    if (value.includes(first)) {
      firstKey = key;
    }
    if (value.includes(second)) {
      secondKey = key;
    }
    if (value.includes(third)) {
      thirdKey = key;
    }
  });
  if (firstKey !== "none" && secondKey !== "none" && thirdKey !== "none") {
    if (
      firstKey !== secondKey &&
      firstKey !== thirdKey &&
      secondKey !== thirdKey
    ) {
      const texturemanager = await textureManager.find({
        [firstKey]: first,
        [secondKey]: second,
        [thirdKey]: third,
      });
      if (texturemanager.length == 0 || texturemanager === undefined)
        res.json({ err: "texture not found" });
      else res.json(texturemanager);
    } else res.json({ err: "filter cannot be the same type as other" });
  } else res.json({ err: "please recheck filter word" });
});
app.get("/textureFilter/:first/:second", async (req, res) => {
  let firstKey = "none";
  let secondKey = "none";
  const { first, second } = req.params;
  Object.entries(wordbag).forEach(([key, value]) => {
    if (value.includes(first)) {
      firstKey = key;
    }
    if (value.includes(second)) {
      secondKey = key;
    }
  });
  if (firstKey !== "none" && secondKey !== "none") {
    if (firstKey !== secondKey) {
      const texturemanager = await textureManager.find({
        [firstKey]: first,
        [secondKey]: second,
      });
      if (texturemanager.length == 0 || texturemanager === undefined)
        res.json({ err: "texture not found" });
      else res.json(texturemanager);
    } else res.json({ err: "filter cannot be the same type as other" });
  } else res.json({ err: "wrong filter word" });
});
app.get("/textureFilter/:first", async (req, res) => {
  let firstKey = "none";
  const { first } = req.params;
  Object.entries(wordbag).forEach(([key, value]) => {
    if (value.includes(first)) {
      firstKey = key;
    }
  });
  if (firstKey !== "none") {
    const texturemanager = await textureManager.find({ [firstKey]: first });
    if (texturemanager.length == 0 || texturemanager === undefined)
      res.json({ err: "texture not found" });
    else res.json(texturemanager);
  } else res.json({ err: "wrong filter word" });
});
//----------------------------------------------------------------------------------Subtype wheel Section ------------------------------------------------------
app.get("/wheel", (req, res) => {
  const path = __dirname + "/data/wheel/WheelList.json";
  const rawdata = fs.readFileSync(path);
  const wheel = JSON.parse(rawdata);
  res.json(wheel);
});
app.put("/wheel/:mainType/:newSubtype", (req, res) => {
  const { mainType, newSubtype } = req.params;
  const path = __dirname + "/data/wheel/WheelList.json";
  const rawdata = fs.readFileSync(path);
  const wheel = JSON.parse(rawdata);

  res.json(wheel);
  wheel.subType[mainType].push({ _id: newSubtype, name: newSubtype });
  console.log(wheel.subType[mainType]);
  const string_wheel = JSON.stringify(wheel);
  fs.writeFile(
    `${__dirname}/data/wheel/` + "WheelList.json",
    string_wheel,
    (err) => {
      if (err) {
        console.log("Cannot write file");
      }
      console.log("JSON csv data is saved.");
    }
  );
});
app.delete("/wheel/:mainType/:subType", (req, res) => {
  const { mainType, subType } = req.params;
  const path = __dirname + "/data/wheel/WheelList.json";
  const rawdata = fs.readFileSync(path);
  const wheel = JSON.parse(rawdata);

  const remain_wheel = wheel.subType[mainType].filter(
    (sub) => sub.name !== subType
  );
  wheel.subType[mainType] = remain_wheel;
  res.json(wheel);
  const string_wheel = JSON.stringify(wheel);
  fs.writeFile(
    `${__dirname}/data/wheel/` + "WheelList.json",
    string_wheel,
    (err) => {
      if (err) {
        console.log("Cannot write file");
      }
      console.log("JSON csv data is saved.");
    }
  );
});

// -------------------------------------------------------------------------------Filtering API Section----------------------------------------------------------
app.get("/data/beaconFilter", (req, res) => {
  const path = __dirname + "/data/wheel/WheelList.json";
  const rawdata = fs.readFileSync(path);
  const wheel = JSON.parse(rawdata);

  let flood_sub = wheel.subType["Flooding"].map(function (i) {
    return i._id;
  });
  res.json({
    Data: [
      {
        mainType: "Flooding",
        subTypes: flood_sub,
      },
    ],
  });
});

app.get("/data/textureFilter", (req, res) => {
  const path = __dirname + "/data/wheel/WheelList.json";
  const rawdata = fs.readFileSync(path);
  const wheel = JSON.parse(rawdata);

  let land_sub = wheel.subType["Landslide"].map(function (i) {
    return i._id;
  });
  res.json({
    Data: [
      {
        mainType: "Landslide",
        subTypes: land_sub,
      },
    ],
  });
});

// -------------------------------------------------------------------------------File API Section----------------------------------------------------------
app.delete("/file/:fileName", async (req, res) => {
  const { fileName } = req.params;
  if (fileName.includes(".csv") || fileName.includes(".CSV")) {
    fs.stat(`${__dirname}/data/csv/${fileName}`, function (err, stats) {
      console.log(stats); //here we got all information of file in stats variable

      if (err) {
        return console.error(err);
      }

      fs.unlink(`${__dirname}/data/csv/${fileName}`, function (err) {
        if (err) console.log("Cannot delete file");
        // if no error, file has been deleted successfully
        console.log("File deleted!");
      });
    });
  } else {
    fs.stat(`${__dirname}/data/textures/${fileName}`, function (err, stats) {
      console.log(stats); //here we got all information of file in stats variable

      if (err) {
        return console.error(err);
      }

      fs.unlink(`${__dirname}/data/textures/${fileName}`, function (err) {
        if (err) console.log("Cannot delete file");
        // if no error, file has been deleted successfully
        console.log("File deleted!");
      });
    });
  }
  res.status(204).end();
});
// -------------------------------------------------------------------------------Homepage API Section----------------------------------------------------------
// welcome message
app.get("/*", (req, res) => {
  // res.status(200).send("Welcome to api express!!!");
  const path = __dirname + "/api.html";
  const file = fs.readFileSync(path);
  res.send(file.toString());
});

// app.use(express.static(path.join(__dirname, "pinmanager", "build")));

// app.get("/*", (req, res) => {
//   console.log();
//   res.sendFile(path.join(__dirname, "pinmanager", "build", "index.html"));
// });

// wrong path error prompt (localhost:5000/asdfghjkl;)
app.use((req, res, next) => {
  var err = new Error("Path not found");
  err.status = 404;
  next(err);
});
