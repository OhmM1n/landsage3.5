import React, { Component } from "react";
import { ToastContainer } from "react-toastify";
import http from "./services/httpService";
import config from "./config.js";
import "react-toastify/dist/ReactToastify.css";
import "./App.css";
import Modalwindow from "./modal";

import "react-bootstrap-table-next/dist/react-bootstrap-table2.min.css";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Collapse from "@material-ui/core/Collapse";
import IconButton from "@material-ui/core/IconButton";
import KeyboardArrowDownIcon from "@material-ui/icons/KeyboardArrowDown";
import KeyboardArrowUpIcon from "@material-ui/icons/KeyboardArrowUp";
import { Tab } from "bootstrap";
import { withStyles } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import { render } from "react-dom";
import TableCell from "@material-ui/core/TableCell";
import CollapseStationtable from "./collapseStationTable";

const StyledCell = withStyles({
  head: {
    fontWeight: 700,
    fontSize: 20,
  },
  root: {
    backgroundColor: "white",
  },
})(TableCell);

class RenderStation extends Component {
  state = { open: false, posts: [], modalState: false };
  //call data from backend
  async componentDidMount() {
    const { data: posts } = await http.get(config.stationEndpoint);
    this.setState({ posts });
  }
  //functon for open and close modal
  handleModalState = (modalState) => {
    modalState = !modalState;
    this.setState({ modalState });
  };
  //fucntion for post payload(new new station) to backend
  handleAdd = async (obj) => {
    const { data: post } = await http.post(config.stationEndpoint, obj);
    console.log(post);
    const posts = [post, ...this.state.posts];
    this.setState({ posts });
  };
  //fucntion for put payload(edit existing  station) to backend
  handleUpdate = async (post) => {
    post.title = "UPDATED";
    //console.log(post);

    const result = await http.put(
      config.stationEndpoint + "/" + post._id,
      post
    );
    //console.log(result);
    const posts = [...this.state.posts];
    const index = posts.map((post) => post._id).indexOf(post._id);
    posts[index] = { ...post };
    //console.log(posts);
    this.setState({ posts });
  };
  //fucntion for delete existing data
  handleDelete = async (post) => {
    const originalPosts = this.state.posts;

    const posts = this.state.posts.filter((p) => p._id !== post._id);
    this.setState({ posts });
    try {
      await http.delete(config.stationEndpoint + "/" + post._id);
    } catch (ex) {
      //expected(404 not found, 400 bad request)
      // display a specific error message
      //console.log(ex.response);
      if (ex.response && ex.response.status === 404) {
        alert("This post has already been deleted");
        //console.log("error:", ex);
      }

      //Unexpected (network down , server down, db down , bug)
      // Display a generic and friendly error message

      this.setState({ posts: originalPosts });
    }
  };
  handleDelData = async (row, file) => {
    //console.log(file.subType);
    const FL = [...row.fileList];
    const newFL = FL.filter(
      (payloadfile) => payloadfile.subType !== file.subType
    );
    //console.log(FL);
    const payload = {
      name: row.name,
      country: row.country,
      lat: row.lat,
      long: row.long,
      _id: row._id,
      fileList: [...newFL],
    };
    try {
      await http.delete(config.fileEndpoint + "/" + file.fileName);
    } catch (ex) {
      //expected(404 not found, 400 bad request)
      // display a specific error message
      //console.log(ex.response);
      if (ex.response && ex.response.status === 404) {
        alert("Something  Went wrong");
        //console.log("error:", ex);
      }
    }

    //console.log(payload);
    this.handleUpdate(payload);
  };
  handleOpen = () => {
    const open = !this.state.open;
    this.setState({ open });
  };

  render() {
    //console.log();
    return (
      <div
        style={{
          // backgroundColor: "#F8F9FA",
          backgroundColor: "#F8F9FA",
          minHeight: "100vh",
          padding: "30px",
        }}
      >
        <ToastContainer />
        <h1>Station List</h1>
        <Modalwindow action="addStation" handleAdd={this.handleAdd} />
        <div>
          <TableContainer
            style={{ backgroundColor: "#F8F9FA" }}
            component={Paper}
          >
            <Table
              // style={{
              //   borderCollapse: "separate",
              //   borderSpacing: "0 10px",
              // }}
              aria-label="simple table"
            >
              <TableHead>
                <TableRow>
                  <StyledCell
                    style={{ fontWeight: 700 }}
                    align="middle"
                  ></StyledCell>
                  <StyledCell align="middle">Station Name</StyledCell>
                  <StyledCell align="middle">Country</StyledCell>
                  <StyledCell align="middle">Latitude</StyledCell>
                  <StyledCell align="middle">Longitude</StyledCell>
                  <StyledCell align="middle">Station Type</StyledCell>
                  <StyledCell align="middle">Action</StyledCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {this.state.posts.map((post) => (
                  <CollapseStationtable
                    key={post._id}
                    post={post}
                    handleUpdate={this.handleUpdate}
                    handleDelete={this.handleDelete}
                    handleDelData={this.handleDelData}
                  />
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </div>
      </div>
    );
  }
}

export default RenderStation;
