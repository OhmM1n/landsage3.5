import React, { Component } from "react";
import Joi from "joi-browser";
import Form from "../common/form";
import http from "../services/httpService";
import { FileUpload } from "../common/fileUpload";
import Progress from "../common/progress";
import Wheel from "../WheelList.json";
import config from "../config";
//component for render form inside modal
class Addpinform extends Form {
  state = {
    data: [],
    fileType: "",
    dataHeader: {
      name: "",
      country: "",
      lat: "",
      long: "",
      maintype: "Flooding",
      subtype: "",
      visualformat: "",
      fileName: "",
      maincustom: "",
      customkey1: "",
      customvalue1: "",
      customkey2: "",
      customvalue2: "",
      customkey3: "",
      customvalue3: "",
      customkey4: "",
      customvalue4: "",
      customkey5: "",
      customvalue5: "",
    },
    errors: {},
    uploadPercentage: 0,
    existData: [],
    Wheel: "",
  };
  async componentDidMount() {
    const { data: Wheel } = await http.get(config.wheelEndpoint);
    this.setState({ Wheel });
    //console.log(Wheel);
  }
  //define data schema
  schema = {
    name: Joi.string().required().label("Station name"),
    country: Joi.string().required().label("Country"),
    lat: Joi.number().min(-90).max(90).required().label("Latitude"),
    long: Joi.number().min(-180).max(180).required().label("Longtitude"),
    maintype: Joi.string().required().label("Main DataType"),
    subtype: Joi.string().required().label("Sub DataType"),
    visualformat: Joi.string().required().label("Visualize Format"),
    fileName: Joi.required(),
    maincustom: Joi.string().allow(""),
    customkey1: Joi.string().allow(""),
    customvalue1: Joi.string().allow(""),
    customkey2: Joi.string().allow(""),
    customvalue2: Joi.string().allow(""),
    customkey3: Joi.string().allow(""),
    customvalue3: Joi.string().allow(""),
    customkey4: Joi.string().allow(""),
    customvalue4: Joi.string().allow(""),
    customkey5: Joi.string().allow(""),
    customvalue5: Joi.string().allow(""),
  };
  // function for posting data(add station)
  handleUpload = async (file) => {
    // auto set input value from file name
    let autoHeader = {
      ...this.state.dataHeader,
      fileName: file.name,
    };
    let fileNameSplit = file.name.replace(/\.[^/.]+$/, "").split("_");
    //console.log(fileNameSplit);
    if (fileNameSplit.length === 5) {
      autoHeader.name = fileNameSplit[4];
      autoHeader.lat = fileNameSplit[1];
      autoHeader.long = fileNameSplit[2];
    }
    this.setState({ dataHeader: autoHeader });

    //this.setState({ uploadPercentage: 0 });
    const formData = new FormData();
    formData.append("file", file);
    //upload csv file to backend
    try {
      const res = await http.post(config.dataEndpoint, formData, {
        headers: {
          "Context-Type": "multipart/form-data",
        },
        onUploadProgress: (progressEvent) => {
          //update progress bar state
          const uploadPercentage =
            (progressEvent.loaded / progressEvent.total) * 100;

          this.setState({ uploadPercentage });
          setTimeout(() => this.setState({ uploadPercentage: 0 }), 1000);
        },
      });
    } catch (err) {
      if (err.response.status === 500) {
        console.log("There was a problem with the server");
      }
    }
  };
  doSubmit = () => {
    let filetype = "";
    switch (true) {
      case this.state.dataHeader.fileName.includes(".csv") ||
        this.state.dataHeader.fileName.includes(".CSV"):
        filetype = "csv";
        break;
      case this.state.dataHeader.fileName.includes(".png") ||
        this.state.dataHeader.fileName.includes(".PNG"):
        filetype = "texture";
        break;
      default:
        filetype = "csv";
    }
    const payload = {
      name: this.state.dataHeader.name,
      country: this.state.dataHeader.country,
      lat: this.state.dataHeader.lat,
      long: this.state.dataHeader.long,
      fileList: [
        {
          mainType:
            this.state.dataHeader.maintype === "Custom"
              ? this.state.dataHeader.maincustom
              : this.state.dataHeader.maintype,
          subType: this.state.dataHeader.subtype,
          visualFormat: this.state.dataHeader.visualformat,
          fileType: filetype,
          fileName: this.state.dataHeader.fileName,
          custom: [
            {
              key: this.state.dataHeader.customkey1,
              value: this.state.dataHeader.customvalue1,
            },
            {
              key: this.state.dataHeader.customkey2,
              value: this.state.dataHeader.customvalue2,
            },
            {
              key: this.state.dataHeader.customkey3,
              value: this.state.dataHeader.customvalue3,
            },
            {
              key: this.state.dataHeader.customkey4,
              value: this.state.dataHeader.customvalue4,
            },
            {
              key: this.state.dataHeader.customkey5,
              value: this.state.dataHeader.customvalue5,
            },
          ],
        },
      ],
    };
    //upload data to the database

    this.props.handleAdd(payload);
    this.props.handleClose();
  };
  rendersubSelect() {
    switch (this.state.dataHeader.maintype) {
      case "Flooding":
        return this.renderSelect(
          "subtype",
          "Sub Data Type",
          this.state.Wheel.subType.Flooding
        );
      case "Landslide":
        return this.renderSelect(
          "subtype",
          "Sub Data Type",
          this.state.Wheel.subType.Landslide
        );
      case "Custom":
        return (
          <div>
            {this.renderInput("maincustom", "Custom Main DataType")}
            {this.renderInput("subtype", "Custom Sub DataType")}
          </div>
        );
    }
  }
  renderVisualtype(fileName) {
    //console.log(fileName.includes(".png"));
    switch (true) {
      case fileName.includes(".csv") || fileName.includes(".CSV"):
        return this.renderSelect(
          "visualformat",
          "Visialize Format",
          Wheel.visualFormat.timeSeries
        );
      case fileName.includes(".png") || fileName.includes(".PNG"):
        return this.renderSelect(
          "visualformat",
          "Visialize Format",
          Wheel.visualFormat.textureMap
        );
    }
  }
  render() {
    //console.log(this.validate());
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          {this.renderInput(
            "name",
            "Station Name",
            "Station name",
            "Enter Station Name"
          )}
          {this.renderSelect("country", "Country", Wheel.country)}
          {this.renderInput("lat", "Latitude", "Latitude", "Enter latitude")}
          {this.renderInput(
            "long",
            "Longtitude",
            "Longtitude",
            "Enter Longtitude"
          )}
          {this.renderSelect(
            "maintype",
            "Main Data Type",
            Wheel.mainType,
            true
          )}
          {this.state.Wheel !== "" ? this.rendersubSelect() : null}

          {/* conditional rendering progress bar */}
          <FileUpload handleUpload={this.handleUpload} />
          {this.state.uploadPercentage === 0 ? null : (
            <Progress percentage={this.state.uploadPercentage} />
          )}
          {this.renderVisualtype(this.state.dataHeader.fileName)}
          <h5>Custom Props(Optional)</h5>
          <div className="form-row">
            <div className="form-group col-6">
              {this.renderInput("customkey1", "Key1", "Key1")}
            </div>
            <div className="form-group col-6">
              {this.renderInput("customvalue1", "Value1", "Value1")}
            </div>
          </div>
          <div className="form-row">
            <div className="form-group col-6">
              {this.renderInput("customkey2", "Key2", "Key2")}
            </div>
            <div className="form-group col-6">
              {this.renderInput("customvalue2", "Value2", "Value2")}
            </div>
          </div>
          <div className="form-row">
            <div className="form-group col-6">
              {this.renderInput("customkey3", "Key3", "Key3")}
            </div>
            <div className="form-group col-6">
              {this.renderInput("customvalue3", "Value3", "Value3")}
            </div>
          </div>
          <div className="form-row">
            <div className="form-group col-6">
              {this.renderInput("customkey4", "Key4", "Key4")}
            </div>
            <div className="form-group col-6">
              {this.renderInput("customvalue4", "Value4", "Value4")}
            </div>
          </div>
          <div className="form-row">
            <div className="form-group col-6">
              {this.renderInput("customkey5", "Key5", "Key5")}
            </div>
            <div className="form-group col-6">
              {this.renderInput("customvalue5", "Value5", "Value5")}
            </div>
          </div>

          <button
            style={{ margin: 10 }}
            disabled={
              this.validate() !== null ||
              this.state.dataHeader.fileName === "" ||
              (this.state.dataHeader.maintype === "Custom" &&
                this.state.dataHeader.maincustom === "")
            }
            className="btn btn-primary float-right"
          >
            Confirm
          </button>
        </form>
      </div>
    );
  }
}

export default Addpinform;
