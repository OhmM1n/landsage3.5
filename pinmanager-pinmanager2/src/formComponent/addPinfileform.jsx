import React, { Component } from "react";
import Joi from "joi-browser";
import Form from "../common/form";
import http from "../services/httpService";
import { FileUpload } from "../common/fileUpload";
//import { mainType, subType, visualFormat } from "../DataTypeSelector.js";
import Wheel from "../WheelList.json";
import config from "../config";
// extend the rendering input and validate function from Form component
class AddPinfileform extends Form {
  // set state according to the original data
  state = {
    _id: this.props.originalState._id,
    data: [],
    dataHeader: {
      maintype: "Flooding",
      subtype: "",
      fileName: "",
      visualformat: "",
      maincustom: "",
      customkey1: "",
      customvalue1: "",
      customkey2: "",
      customvalue2: "",
      customkey3: "",
      customvalue3: "",
      customkey4: "",
      customvalue4: "",
      customkey5: "",
      customvalue5: "",
    },
    errors: {},
    Wheel: "",
  };
  async componentDidMount() {
    const { data: Wheel } = await http.get(config.wheelEndpoint);
    this.setState({ Wheel });
  }
  // define schema for validate input data
  schema = {
    maintype: Joi.string().required().label("Main DataType"),
    subtype: Joi.string().required().label("Sub Data Type"),
    visualformat: Joi.string().required().label("Visualize Format"),
    fileName: Joi.required(),
    maincustom: Joi.string().allow(""),
    customkey1: Joi.string().allow(""),
    customvalue1: Joi.string().allow(""),
    customkey2: Joi.string().allow(""),
    customvalue2: Joi.string().allow(""),
    customkey3: Joi.string().allow(""),
    customvalue3: Joi.string().allow(""),
    customkey4: Joi.string().allow(""),
    customvalue4: Joi.string().allow(""),
    customkey5: Joi.string().allow(""),
    customvalue5: Joi.string().allow(""),
  };
  // function for upload csv file
  handleUpload = async (file) => {
    const dataHeader = { ...this.state.dataHeader, fileName: file.name };
    //console.log(dataHeader);
    this.setState({ dataHeader });

    const formData = new FormData();
    formData.append("file", file);
    //upload data to backend
    try {
      const res = await http.post(config.dataEndpoint, formData, {
        headers: {
          "Context-Type": "multipart/form-data",
        },
      });
    } catch (err) {
      if (err.response.status === 500) {
        //console.log("There was a problem with the server");
      }
    }
  };
  //function for put the data(update existing data )
  doSubmit = () => {
    //submit edit data to database
    //console.log("Submitted");
    let filetype = "";
    switch (true) {
      case this.state.dataHeader.fileName.includes(".csv") ||
        this.state.dataHeader.fileName.includes(".CSV"):
        filetype = "csv";
        break;
      case this.state.dataHeader.fileName.includes(".png") ||
        this.state.dataHeader.fileName.includes(".PNG"):
        filetype = "texture";
        break;
      default:
        filetype = "csv";
    }
    const FL = [...this.props.originalState.fileList];
    if (FL.some((file) => file.subType === this.state.dataHeader.subtype)) {
      FL.map((file) =>
        file.subType === this.state.dataHeader.subtype
          ? (file.fileName = this.state.dataHeader.fileName)
          : null
      );
    } else {
      FL.push({
        mainType:
          this.state.dataHeader.maintype === "Custom"
            ? this.state.dataHeader.maincustom
            : this.state.dataHeader.maintype,
        subType: this.state.dataHeader.subtype,
        visualFormat: this.state.dataHeader.visualformat,
        fileType: filetype,
        fileName: this.state.dataHeader.fileName,
        custom: [
          {
            key: this.state.dataHeader.customkey1,
            value: this.state.dataHeader.customvalue1,
          },
          {
            key: this.state.dataHeader.customkey2,
            value: this.state.dataHeader.customvalue2,
          },
          {
            key: this.state.dataHeader.customkey3,
            value: this.state.dataHeader.customvalue3,
          },
          {
            key: this.state.dataHeader.customkey4,
            value: this.state.dataHeader.customvalue4,
          },
          {
            key: this.state.dataHeader.customkey5,
            value: this.state.dataHeader.customvalue5,
          },
        ],
      });
    }

    //console.log(FL);
    const payload = {
      name: this.props.originalState.name,
      country: this.props.originalState.country,
      lat: this.props.originalState.lat,
      long: this.props.originalState.long,
      _id: this.state._id,
      fileList: [...FL],
    };
    //console.log(payload);
    this.props.handleUpdate(payload);
    this.props.handleClose();
  };
  rendersubSelect() {
    switch (this.state.dataHeader.maintype) {
      case "Flooding":
        return this.renderSelect(
          "subtype",
          "Sub DataType",
          this.state.Wheel.subType.Flooding
        );
      case "Landslide":
        return this.renderSelect(
          "subtype",
          "Sub DataType",
          this.state.Wheel.subType.Landslide
        );
      case "Custom":
        return (
          <div>
            {this.renderInput("maincustom", "Custom Main DataType")}
            {this.renderInput("subtype", "Custom Sub DataType")}
          </div>
        );
    }
  }
  renderVisualtype(fileName) {
    //console.log(fileName.includes(".png"));
    switch (true) {
      case fileName.includes(".csv"):
        return this.renderSelect(
          "visualformat",
          "Visialize Format",
          Wheel.visualFormat.timeSeries
        );
      case fileName.includes(".png"):
        return this.renderSelect(
          "visualformat",
          "Visialize Format",
          Wheel.visualFormat.textureMap
        );
    }
  }

  render() {
    //console.log(this.validate());
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          {this.renderSelect("maintype", "Main DataType", Wheel.mainType, true)}
          {this.state.Wheel !== "" ? this.rendersubSelect() : null}

          <FileUpload
            originalfileName={this.state.dataHeader.fileName}
            handleUpload={this.handleUpload}
          />
          {this.renderVisualtype(this.state.dataHeader.fileName)}
          <h4>Custom Props(Optional)</h4>
          <div className="form-row">
            <div className="form-group col-6">
              {this.renderInput("customkey1", "Key1", "Key1")}
            </div>
            <div className="form-group col-6">
              {this.renderInput("customvalue1", "Value1", "Value1")}
            </div>
          </div>
          <div className="form-row">
            <div className="form-group col-6">
              {this.renderInput("customkey2", "Key2", "Key2")}
            </div>
            <div className="form-group col-6">
              {this.renderInput("customvalue2", "Value2", "Value2")}
            </div>
          </div>
          <div className="form-row">
            <div className="form-group col-6">
              {this.renderInput("customkey3", "Key3", "Key3")}
            </div>
            <div className="form-group col-6">
              {this.renderInput("customvalue3", "Value3", "Value3")}
            </div>
          </div>
          <div className="form-row">
            <div className="form-group col-6">
              {this.renderInput("customkey4", "Key4", "Key4")}
            </div>
            <div className="form-group col-6">
              {this.renderInput("customvalue4", "Value4", "Value4")}
            </div>
          </div>
          <div className="form-row">
            <div className="form-group col-6">
              {this.renderInput("customkey5", "Key5", "Key5")}
            </div>
            <div className="form-group col-6">
              {this.renderInput("customvalue5", "Value5", "Value5")}
            </div>
          </div>
          {this.renderButton("Update")}
        </form>
      </div>
    );
  }
}

export default AddPinfileform;
