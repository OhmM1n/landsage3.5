import React, { Component } from "react";
import Joi from "joi-browser";
import Form from "../common/form";
import http from "../services/httpService";
import { FileUpload } from "../common/fileUpload";
//import { mainType, subType, visualFormat } from "../DataTypeSelector.js";
import Wheel from "../WheelList.json";
import config from "../config";
// extend the rendering input and validate function from Form component
class EditPindataform extends Form {
  // set state according to the original data
  state = {
    _id: this.props.originalState._id,
    data: [],
    dataHeader: {
      maintype: this.props.originalFile.mainType,
      subtype: this.props.originalFile.subType,
      visualformat: this.props.originalFile.visualFormat,
      fileName: this.props.originalFile.fileName,
      maincustom: "",
      customkey1: "",
      customvalue1: "",
      customkey2: "",
      customvalue2: "",
      customkey3: "",
      customvalue3: "",
      customkey4: "",
      customvalue4: "",
      customkey5: "",
      customvalue5: "",
    },
    fileList: this.props.originalState.fileList,
    errors: {},
    Wheel: "",
  };
  async componentDidMount() {
    const { data: Wheel } = await http.get(config.wheelEndpoint);
    const mainTypeList = ["Flooding", "Landslide"];
    const dataHeader = {
      maintype: mainTypeList.includes(this.props.originalFile.mainType)
        ? this.props.originalFile.mainType
        : "Custom",
      subtype: this.props.originalFile.subType,
      visualformat: this.props.originalFile.visualFormat,
      fileName: this.props.originalFile.fileName,
      maincustom: mainTypeList.includes(this.props.originalFile.mainType)
        ? ""
        : this.props.originalFile.mainType,
      customkey1:
        this.props.originalFile.custom.length == 0 ||
        this.props.originalFile.custom === undefined
          ? ""
          : this.props.originalFile.custom[0].key,
      customvalue1:
        this.props.originalFile.custom.length == 0 ||
        this.props.originalFile.custom === undefined
          ? ""
          : this.props.originalFile.custom[0].value,
      customkey2:
        this.props.originalFile.custom.length == 0 ||
        this.props.originalFile.custom === undefined
          ? ""
          : this.props.originalFile.custom[1].key,
      customvalue2:
        this.props.originalFile.custom.length == 0 ||
        this.props.originalFile.custom === undefined
          ? ""
          : this.props.originalFile.custom[1].value,
      customkey3:
        this.props.originalFile.custom.length == 0 ||
        this.props.originalFile.custom === undefined
          ? ""
          : this.props.originalFile.custom[2].key,
      customvalue3:
        this.props.originalFile.custom.length == 0 ||
        this.props.originalFile.custom === undefined
          ? ""
          : this.props.originalFile.custom[2].value,
      customkey4:
        this.props.originalFile.custom.length == 0 ||
        this.props.originalFile.custom === undefined
          ? ""
          : this.props.originalFile.custom[3].key,
      customvalue4:
        this.props.originalFile.custom.length == 0 ||
        this.props.originalFile.custom === undefined
          ? ""
          : this.props.originalFile.custom[3].value,
      customkey5:
        this.props.originalFile.custom.length == 0 ||
        this.props.originalFile.custom === undefined
          ? ""
          : this.props.originalFile.custom[4].key,
      customvalue5:
        this.props.originalFile.custom.length == 0 ||
        this.props.originalFile.custom === undefined
          ? ""
          : this.props.originalFile.custom[4].value,
    };
    this.setState({ Wheel, dataHeader });
  }
  // define schema for validate input data
  schema = {
    maintype: Joi.string().required().label("Main DataType"),
    subtype: Joi.string().required().label("Sub Data Type"),
    visualformat: Joi.string().required().label("Visualize Format"),
    fileName: Joi.required(),
    maincustom: Joi.string().allow(""),
    customkey1: Joi.string().allow(""),
    customvalue1: Joi.string().allow(""),
    customkey2: Joi.string().allow(""),
    customvalue2: Joi.string().allow(""),
    customkey3: Joi.string().allow(""),
    customvalue3: Joi.string().allow(""),
    customkey4: Joi.string().allow(""),
    customvalue4: Joi.string().allow(""),
    customkey5: Joi.string().allow(""),
    customvalue5: Joi.string().allow(""),
  };
  // function for upload csv file
  handleUpload = async (file) => {
    const dataHeader = { ...this.state.dataHeader, fileName: file.name };
    //console.log(dataHeader);
    this.setState({ dataHeader });

    const formData = new FormData();
    formData.append("file", file);
    //upload data to backend
    try {
      const res = await http.post(config.dataEndpoint, formData, {
        headers: {
          "Context-Type": "multipart/form-data",
        },
      });
    } catch (err) {
      if (err.response.status === 500) {
        console.log("There was a problem with the server");
      }
    }
  };
  //function for put the data(update existing data )
  doSubmit = () => {
    //submit edit data to database
    let filetype = "";
    switch (true) {
      case this.state.dataHeader.fileName.includes(".csv") ||
        this.state.dataHeader.fileName.includes(".CSV"):
        filetype = "csv";
        break;
      case this.state.dataHeader.fileName.includes(".png") ||
        this.state.dataHeader.fileName.includes(".PNG"):
        filetype = "texture";
        break;
      default:
        filetype = "csv";
    }
    //console.log("Submitted");
    const FL = [...this.props.originalState.fileList];
    FL.map((file) =>
      file.subType === this.props.originalFile.subType
        ? ((file.fileName = this.state.dataHeader.fileName),
          (file.mainType =
            this.state.dataHeader.maintype === "Custom"
              ? this.state.dataHeader.maincustom
              : this.state.dataHeader.maintype),
          (file.visualFormat = this.state.dataHeader.visualformat),
          (file.fileType = filetype),
          (file.subType = this.state.dataHeader.subtype),
          (file.custom = [
            {
              key: this.state.dataHeader.customkey1,
              value: this.state.dataHeader.customvalue1,
            },
            {
              key: this.state.dataHeader.customkey2,
              value: this.state.dataHeader.customvalue2,
            },
            {
              key: this.state.dataHeader.customkey3,
              value: this.state.dataHeader.customvalue3,
            },
            {
              key: this.state.dataHeader.customkey4,
              value: this.state.dataHeader.customvalue4,
            },
            {
              key: this.state.dataHeader.customkey5,
              value: this.state.dataHeader.customvalue5,
            },
          ]))
        : null
    );
    const payload = {
      name: this.props.originalState.name,
      country: this.props.originalState.country,
      lat: this.props.originalState.lat,
      long: this.props.originalState.long,
      _id: this.props.originalState._id,
      fileList: [...FL],
    };
    //console.log(payload);
    this.props.handleUpdate(payload);
    this.props.handleClose();
  };
  rendersubSelect() {
    const existSubtype = this.state.fileList.map(
      (file) => file.subType !== this.originalsubType
    );

    //console.log(existSubtype);
    const filterLandslideSubtype = [...this.state.Wheel.subType.Landslide];
    // console.log(
    //   filterLandslideSubtype.filter((x) => !existSubtype.includes(x._id))
    // );
    switch (this.state.dataHeader.maintype) {
      case "Flooding":
        return this.renderSelect(
          "subtype",
          "Sub Data Type",
          this.state.Wheel.subType.Flooding.filter(
            (x) => !existSubtype.includes(x._id)
          )
        );
      case "Landslide":
        return this.renderSelect(
          "subtype",
          "Sub Data Type",
          this.state.Wheel.subType.Landslide.filter(
            (x) => !existSubtype.includes(x._id)
          )
        );
      case "Custom":
        return (
          <div>
            {this.renderInput("maincustom", "Custom Main DataType")}
            {this.renderInput("subtype", "Custom Sub DataType")}
          </div>
        );
    }
  }
  renderVisualtype(fileName) {
    //console.log(fileName.includes(".png"));
    switch (true) {
      case fileName.includes(".csv"):
        return this.renderSelect(
          "visualformat",
          "Visialize Format",
          Wheel.visualFormat.timeSeries
        );
      case fileName.includes(".png"):
        return this.renderSelect(
          "visualformat",
          "Visialize Format",
          Wheel.visualFormat.textureMap
        );
    }
  }

  render() {
    //console.log(this.state.Wheel);
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          {this.renderSelect("maintype", "Main Data Type", Wheel.mainType)}
          {this.state.Wheel !== "" ? this.rendersubSelect() : null}
          <FileUpload
            originalfileName={this.state.dataHeader.fileName}
            handleUpload={this.handleUpload}
          />
          {this.renderVisualtype(this.state.dataHeader.fileName)}
          <h5>Custom Props(Optional)</h5>
          <div className="form-row">
            <div className="form-group col-6">
              {this.renderInput("customkey1", "Key1", "Key1")}
            </div>
            <div className="form-group col-6">
              {this.renderInput("customvalue1", "Value1", "Value1")}
            </div>
          </div>
          <div className="form-row">
            <div className="form-group col-6">
              {this.renderInput("customkey2", "Key2", "Key2")}
            </div>
            <div className="form-group col-6">
              {this.renderInput("customvalue2", "Value2", "Value2")}
            </div>
          </div>
          <div className="form-row">
            <div className="form-group col-6">
              {this.renderInput("customkey3", "Key3", "Key3")}
            </div>
            <div className="form-group col-6">
              {this.renderInput("customvalue3", "Value3", "Value3")}
            </div>
          </div>
          <div className="form-row">
            <div className="form-group col-6">
              {this.renderInput("customkey4", "Key4", "Key4")}
            </div>
            <div className="form-group col-6">
              {this.renderInput("customvalue4", "Value4", "Value4")}
            </div>
          </div>
          <div className="form-row">
            <div className="form-group col-6">
              {this.renderInput("customkey5", "Key5", "Key5")}
            </div>
            <div className="form-group col-6">
              {this.renderInput("customvalue5", "Value5", "Value5")}
            </div>
          </div>

          {this.renderButton("Update")}
        </form>
      </div>
    );
  }
}

export default EditPindataform;
