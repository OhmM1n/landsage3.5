import React, { Component } from "react";
import { ToastContainer } from "react-toastify";
import http from "./services/httpService";
import config from "./config.js";
import "react-toastify/dist/ReactToastify.css";
import "./App.css";
import Modalwindow from "./modal";
import { confirmAlert } from "react-confirm-alert";
import "react-confirm-alert/src/react-confirm-alert.css";

import "react-bootstrap-table-next/dist/react-bootstrap-table2.min.css";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Collapse from "@material-ui/core/Collapse";
import IconButton from "@material-ui/core/IconButton";
import KeyboardArrowDownIcon from "@material-ui/icons/KeyboardArrowDown";
import KeyboardArrowUpIcon from "@material-ui/icons/KeyboardArrowUp";
import { Tab } from "bootstrap";
import { withStyles } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import { render } from "react-dom";
import TableCell from "@material-ui/core/TableCell";
import SearchBar from "./common/searchBar";

const StyledCell = withStyles({
  head: {
    fontWeight: 700,
    fontSize: 20,
  },
  root: {
    backgroundColor: "white",
  },
})(TableCell);

class RenderFileManager extends Component {
  state = { fileList: [], searchQuery: "" };

  async componentDidMount() {
    const { data } = await http.get(config.dataEndpoint);
    this.setState({ fileList: data.fileList });
  }

  handleDeleteFile = async (filename) => {
    const fileList = this.state.fileList.filter((file) => file !== filename);
    this.setState({ fileList });
    try {
      await http.delete(config.fileEndpoint + "/" + filename);
    } catch (ex) {
      //expected(404 not found, 400 bad request)
      // display a specific error message
      //console.log(ex.response);
      if (ex.response && ex.response.status === 404) {
        alert("Something  Went wrong");
        //console.log("error:", ex);
      }
    }
  };
  handleSearch = (search) => {
    this.setState({ searchQuery: search });
  };
  render() {
    var filtered_search = this.state.fileList;
    console.log(this.state.fileList);
    const { searchQuery } = this.state;
    if (searchQuery) {
      filtered_search = this.state.fileList.filter((file) =>
        file.toLowerCase().includes(searchQuery.toLowerCase())
      );
    }
    return (
      <React.Fragment>
        <ToastContainer />

        <div className="container">
          <h1 style={{ margin: "15px" }}>File List</h1>{" "}
          <SearchBar
            value={searchQuery}
            onChange={this.handleSearch}
            place={"Search File Name ..."}
          />
          <TableContainer
            style={{ backgroundColor: "#F8F9FA" }}
            component={Paper}
          >
            <Table aria-label="simple table">
              <TableHead>
                <TableRow>
                  <StyledCell align="middle">File Name</StyledCell>
                  <StyledCell align="middle">Type</StyledCell>
                  <StyledCell align="middle">Delete</StyledCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {filtered_search.map((file) => (
                  <React.Fragment>
                    <TableRow>
                      <TableCell>{file}</TableCell>
                      <TableCell>{file.split(".").pop()}</TableCell>
                      <TableCell>
                        <button
                          className="btn btn-danger btn-sm"
                          onClick={() =>
                            confirmAlert({
                              title: `Delete confirmation`,
                              message: `Are you cure to delete \"${file}\"?`,
                              buttons: [
                                {
                                  label: "Yes",
                                  onClick: () => {
                                    this.handleDeleteFile(file);
                                  },
                                },
                                {
                                  label: "No",
                                  onClick: () => {},
                                },
                              ],
                              "&.react-confirm-alert-overlay": {
                                zIndex: 99999,
                              },
                            })
                          }
                        >
                          Delete
                        </button>
                      </TableCell>
                    </TableRow>
                  </React.Fragment>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </div>
      </React.Fragment>
    );
  }
}

export default RenderFileManager;
